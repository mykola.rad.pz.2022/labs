var deleteButton;

function openForm() {
  document.querySelector(".form-header h2").textContent = "Add Student";
  document.querySelector('.button-container input[type="submit"]').value =
    "Create";
  document.getElementById("form-window").style.display = "block";
}

function throwWarning(button) {
  var row = button.parentNode.parentNode;
  var cells = row.querySelectorAll("td");
  deleteButton = button;

  var studentName = cells[2].textContent;

  document.getElementById("warning-text").textContent =
    "Are you sure you want to delete user " + studentName + "?";

  document.getElementById("warning").style.display = "block";
}

function closeWarning() {
  document.getElementById("warning").style.display = "none";
}

function closeForm() {
  var form = document.querySelector(".add-form");
  document.getElementById("form-window").style.display = "none";
  form.reset();
}

document.addEventListener("DOMContentLoaded", function () {
  var form = document.querySelector(".add-form");
  var table = document.querySelector(".students-data");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    var isEditing =
      document.getElementById("form-name").textContent == "Edit Student";

    if (isEditing) {
      editStudentData();
    } else {
      addStudentData();
    }
  });

  function addStudentData() {
    var id = document.getElementById("custId").value;
    var groupName = document.getElementById("groupInput").value;
    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    var studentName = firstName + " " + lastName;
    var gender = document.getElementById("genderInput").value;
    var birthday = document.getElementById("birthday").value;

    var newRow = document.createElement("tr");
    newRow.innerHTML = `
      <td><input type="checkbox" /></td>
      <td>${groupName}</td>
      <td>${studentName}</td>
      <td>${gender}</td>
      <td>${birthday}</td>
      <td><i class="fa-solid fa-circle"></i></i></td>
      <td>
        <button onclick="editStudent(this)"><i class="fa-solid fa-pencil"></i></button>
        <button onclick="throwWarning(this)">&#215</button>
      </td>
    `;

    const data = {
      id: id,
      group: groupName,
      firstName: firstName,
      lastName: lastName,
      gender: gender,
      birthday: birthday,
    };

    const jsonData = JSON.stringify(data);
    console.log(jsonData);
    $.ajax({
      url: "http://localhost:3000/addUser.php",
      type: "POST",
      dataType: "json",
      data: jsonData,
      success: function (response) {
        console.log("Запит успішно відправлено");
        table.appendChild(newRow);
        form.reset();

        closeForm();
      },
      error: function (xhr, error) {
        console.error("Виникла помилка при відправці запиту:", error);
        var errorMessage = xhr.responseJSON.message;

        alert(errorMessage);
      },
    });
  }

  function editStudentData() {
    var rowIndex = parseInt(
      document.getElementById("form-window").getAttribute("data-row-index")
    );
    var row = document.querySelector(".students-data").rows[rowIndex];

    var groupName = document.getElementById("groupInput").value;
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var studentName = firstName + " " + lastName;
    var gender = document.getElementById("genderInput").value;
    var birthday = document.getElementById("birthday").value;
    const data = {
      group: groupName,
      firstName: firstName,
      lastName: lastName,
      gender: gender,
      birthday: birthday,
    };

    const jsonData = JSON.stringify(data);
    console.log(jsonData);
    $.ajax({
      url: "http://localhost:3000/addUser.php",
      type: "POST",
      dataType: "json",
      data: jsonData,
      success: function (response) {
        console.log("Запит успішно відправлено");
        row.cells[1].textContent = groupName;
        row.cells[2].textContent = studentName;
        row.cells[3].textContent = gender;
        row.cells[4].textContent = birthday;
        form.reset();

        closeForm();
      },
      error: function (xhr, error) {
        console.error("Виникла помилка при відправці запиту:", error);
        var errorMessage = xhr.responseJSON.message;

        alert(errorMessage);
      },
    });
  }
});

function deleteStudent() {
  var row = deleteButton.parentNode.parentNode;

  row.parentNode.removeChild(row);

  closeWarning();
}

function editStudent(button) {
  var row = button.parentNode.parentNode;
  var cells = row.querySelectorAll("td");

  var groupName = cells[1].textContent;
  var studentName = cells[2].textContent;
  var gender = cells[3].textContent;
  var birthday = cells[4].textContent;

  openForm();

  document
    .getElementById("form-window")
    .setAttribute("data-row-index", row.rowIndex);

  document.getElementById("groupInput").value = groupName;
  document.getElementById("firstName").value = studentName.split(" ")[0];
  document.getElementById("lastName").value = studentName.split(" ")[1];
  document.getElementById("genderInput").value = gender;
  document.getElementById("birthday").value = birthday;

  document.querySelector(".form-header h2").textContent = "Edit Student";
  document.querySelector('.button-container input[type="submit"]').value =
    "Save";
}
