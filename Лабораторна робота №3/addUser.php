<?php
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");  

function validateData(&$response, $data) {
    try {
        if (in_array(null, $data)) {
            $response = ['message' => 'All fields should be present'];
        } else if (!validateBirthday($data['birthday'])) {
            $response = ['message' => 'Invalid birthday format or age! Age must be between 16 and 60 years old!'];
        } else if (!validateName($data['lastName'])) {
          $response = ['message' => 'Invalid last name! First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed!'];
        } else if (!validateName($data['firstName'])) {
          $response = ['message' => 'Invalid first name! First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed!'];
        }else {
            $response = ['message' => $data];
            return true;
        }
   } catch (Exception $e) {
        error_log($e);
        $response = ['message' => 'Something went wrong'];
   }
    return false;
}

function validateName($name) {
  $nameValue = trim($name);
  $namePattern = '/^[A-Z][a-z\'-]*$/';

  if (!preg_match($namePattern, $nameValue)) 
    return false;
  return true;
}

function validateBirthday($birthday) {
    $age = (new DateTime())->diff(new DateTime($birthday))->y;
    return ($age >= 16 && $age <= 70);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    http_response_code(200);
    $jsonData = json_decode(file_get_contents('php://input'), true);

    $data = [
        'group'    => isset($jsonData['group']) ? $jsonData['group'] : null,
        'firstName'    => isset($jsonData['firstName']) ? $jsonData['firstName'] : null,
        'lastName'     => isset($jsonData['lastName']) ? $jsonData['lastName'] : null,
        'gender'   => isset($jsonData['gender']) ? $jsonData['gender'] : null,
        'birthday' => isset($jsonData['birthday']) ? $jsonData['birthday'] : null
    ];

foreach ($data as $key => $value) {
    error_log("$key: $value");
}

    $response = [];

    if(!validateData($response, $data)) 
    http_response_code(400);
    echo json_encode($response);
} else {
    http_response_code(405);
    $response = ['message' => 'Invalid request method.'];
    echo json_encode($response);
}

?>