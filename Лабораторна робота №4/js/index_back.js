var deleteButton;
class Student {
  id;
  st_group;
  firstName;
  lastName;
  gender;
  birthday;
  st_status;

  constructor(array) {
    this.id = array[0];
    this.st_group = array[1];
    this.firstName = array[2];
    this.lastName = array[3];
    this.gender = array[4];
    this.birthday = array[5];
    this.st_status = array[6];
  }
}

function openForm() {
  if (localStorage.getItem("name") != "Not registered") {
    document.querySelector(".form-header h2").textContent = "Add Student";
    document.querySelector('.button-container input[type="submit"]').value =
      "Create";
    document.getElementById("form-window").style.display = "block";
  }
}

function closeForm() {
  var form = document.querySelector(".add-form");
  document.getElementById("form-window").style.display = "none";
  form.reset();
}

function changeUserState() {
  var userName = localStorage.getItem("name");
  console.log(userName);
  var dropdownContent = document.getElementById("Profile");

  document.getElementById("username").innerText = userName;

  if (userName != "Not registered") {
    var profile = document.createElement("a");
    profile.href = "#";
    profile.textContent = "Profile";

    var logout = document.createElement("a");
    logout.href = "#";
    logout.textContent = "Log out";

    dropdownContent.innerHTML = "";

    dropdownContent.appendChild(profile);
    dropdownContent.appendChild(logout);
  } else {
    document.getElementById("username").innerText = userName;

    var signUp = document.createElement("a");
    signUp.href = "sign-up.html";
    signUp.textContent = "Sign up";

    var logIn = document.createElement("a");
    logIn.href = "log-in.html";
    logIn.textContent = "Log in";

    dropdownContent.innerHTML = "";

    dropdownContent.appendChild(logIn);
    dropdownContent.appendChild(signUp);
  }
}

function createRow(student) {
  console.log(student);
  var studentName = student.firstName + " " + student.lastName;

  var newRow = document.createElement("tr");
  newRow.innerHTML = `
    <td><input type="checkbox" class="delete-checkbox" value="${
      student.id
    }" /></td>
    <td>${student.st_group}</td>
    <td>${studentName}</td>
    <td>${student.gender}</td>
    <td>${formatDate(student.birthday.toString())}</td>
    <td><i id="status" class="fa-solid fa-circle"></i></td>
    <td>
      <button onclick="editStudent(this)"><i class="fa-solid fa-pencil"></i></button>
      <button onclick="throwWarning(this)">&#215</button>
    </td>
  `;
  return newRow;
}

function editRow(row, user) {
  let studentName = user.firstName + " " + user.lastName;
  row.cells[0].value = user.id;
  row.cells[1].textContent = user.st_group;
  row.cells[2].textContent = studentName;
  row.cells[3].textContent = user.gender;
  row.cells[4].textContent = formatDate(user.birthday.toString());
}

function formatDate(dateString) {
  const date = new Date(dateString);

  const day = date.getDate().toString().padStart(2, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const year = date.getFullYear();

  return `${day}.${month}.${year}`;
}

function parseDate(dateString) {
  const [day, month, year] = dateString.split(".");

  const date = new Date(`${year}-${month}-${day}`);

  return date;
}

function ChangeStatus(student) {
  const statusIcon = document.getElementById("status");

  if (student.st_status) {
    statusIcon.classList.add("status-active");
    statusIcon.classList.remove("status-inactive");
  } else {
    statusIcon.classList.add("status-inactive");
    statusIcon.classList.remove("status-active");
  }
}

function editStudent(button) {
  if (localStorage.getItem("name") != "Not registered") {
    var row = button.parentNode.parentNode;
    var cells = row.querySelectorAll("td");

    let id = cells[0].querySelector("input").value;
    console.log(id);
    var groupName = cells[1].textContent;
    var studentName = cells[2].textContent;
    var gender = cells[3].textContent;
    var birthday = parseDate(cells[4].textContent).toISOString().split("T")[0];
    openForm();

    document
      .getElementById("form-window")
      .setAttribute("data-row-index", row.rowIndex);
    document.getElementById("studentId").value = id;
    document.getElementById("groupInput").value = groupName;
    document.getElementById("firstName").value = studentName.split(" ")[0];
    document.getElementById("lastName").value = studentName.split(" ")[1];
    document.getElementById("genderInput").value =
      gender == "M" ? "Male" : "Female";
    document.getElementById("birthday").value = birthday;

    document.querySelector(".form-header h2").textContent = "Edit Student";
    document.querySelector('.button-container input[type="submit"]').value =
      "Save";
  }
}

document.addEventListener("DOMContentLoaded", function () {
  changeUserState();
  var form = document.querySelector(".add-form");
  var table = document.querySelector(".students-data");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    saveStudent(table);
  });
});

function saveStudent(table) {
  var isEditing =
    document.getElementById("form-name").textContent == "Edit Student";

  let student = new Student([
    document.getElementById("studentId").value,
    document.getElementById("groupInput").value,
    document.getElementById("firstName").value,
    document.getElementById("lastName").value,
    document.getElementById("genderInput").value == "Male" ? "M" : "F",
    document.getElementById("birthday").value,
    false,
  ]);

  const data = {
    id: student.id,
    group: student.st_group,
    firstName: student.firstName,
    lastName: student.lastName,
    gender: student.gender,
    birthday: student.birthday,
  };

  const jsonData = JSON.stringify(data);
  console.log(jsonData);
  $.ajax({
    url: "http://localhost:3000/php/add_editStudent.php",
    type: "POST",
    dataType: "json",
    data: jsonData,
    success: function (response) {
      console.log("Запит успішно відправлено");
      if (isEditing) {
        var rowIndex = parseInt(
          document.getElementById("form-window").getAttribute("data-row-index")
        );
        var row = document.querySelector(".students-data").rows[rowIndex];
        editRow(row, student);
      } else {
        student.id = response.message.id;
        let newRow = createRow(student);
        table.appendChild(newRow);
      }

      ChangeStatus(student);
      closeForm();
    },
    error: function (xhr, error) {
      console.error("Виникла помилка при відправці запиту:", error);
      var errorMessage = xhr.responseJSON.message;

      alert(errorMessage);
    },
  });
}

function selectAll(checkbox) {
  Array.from($(".delete-checkbox")).forEach((element) => {
    element.checked = checkbox.checked;
  });
}

function throwWarning(button) {
  if (localStorage.getItem("name") != "Not registered") {
    var row = button.parentNode.parentNode;
    var cells = row.querySelectorAll("td");
    deleteButton = button;

    var studentName = cells[2].textContent;

    document.getElementById("warning-text").textContent =
      "Are you sure you want to delete user " + studentName + "?";

    document.getElementById("warning").style.display = "block";
  }
}

function closeWarning() {
  document.getElementById("warning").style.display = "none";
}

(() => {
  $.ajax({
    url: "http://localhost:3000/php/loadStudents.php",
    type: "GET",
    dataType: "json",
    success: function (response) {
      response.forEach((studentJson) => {
        const student = JSON.parse(studentJson);
        student.st_status = student.st_status === "1";
        let newRow = createRow(student);
        let table = document.querySelector(".students-data");
        table.appendChild(newRow);
      });
    },
    error: function (error) {
      console.error("Виникла помилка при відправці запиту:", error);
      var errorMessage = xhr.responseJSON.message;
      alert(errorMessage);
    },
  });
})();

function deleteStudent() {
  $.ajax({
    url: "http://localhost:3000/php/deleteStudent.php",
    type: "POST",
    dataType: "json",
    data: JSON.stringify({
      id: deleteButton.parentNode.parentNode.children[0].children[0].value,
    }),

    success: function (response) {
      let row = deleteButton.parentNode.parentNode;
      row.parentNode.removeChild(row);
    },
    error: function (error) {
      console.error("Виникла помилка при відправці запиту:", error);
      var errorMessage = xhr.responseJSON.message;
      alert(errorMessage);
    },
  });

  closeWarning();
}

window.addEventListener("beforeunload", function (event) {
  localStorage.setItem("name", "Not registered");
});
