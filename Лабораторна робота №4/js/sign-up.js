document.addEventListener("DOMContentLoaded", function () {
  var form = document.querySelector(".register-form");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    const data = {
      firstName: document.getElementById("firstName").value,
      lastName: document.getElementById("lastName").value,
      birthday: document.getElementById("birthday").value,
      userName: document.getElementById("userName").value,
      password: document.getElementById("password").value,
      confirmPassword: document.getElementById("confirmPassword").value,
    };

    const jsonData = JSON.stringify(data);

    $.ajax({
      url: "http://localhost:3000/php/sign-up.php",
      type: "POST",
      dataType: "json",
      data: jsonData,
      success: function (response) {
        console.log("Запит успішно відправлено");
        localStorage.setItem("loggedIn", "true");
        localStorage.setItem("name", data.userName);
        setTimeout(() => {
          window.location.href = "http://localhost:3000/html/index.html";
        }, 3000);
      },
      error: function (xhr, error) {
        console.error("Виникла помилка при відправці запиту:", error);
        var errorMessage = xhr.responseJSON.message;

        alert(errorMessage);
      },
    });
  });
});
