<?php
include "./db.php";

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type"); 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  http_response_code(200);
  $jsonData = json_decode(file_get_contents('php://input'), true);

  $id = isset($jsonData['id']) ? $jsonData['id'] : -1;
  error_log("ID: $id");
  $response = [];

  if($id == -1) {
      http_response_code(400);
      $response = ['message' => 'ID should be present'];
  } else if(!DeleteStudentFromDB($id)) {
      http_response_code(501);
      $response = ['message' => 'Unable to perform the operation'];
  } else {
      error_log("User deleted successfully");
       $response = ['message' => "User was deleted successfully"];
  }
  echo json_encode($response);
} else {
  http_response_code(405);
  $response = ['message' => 'Invalid request method.'];
  echo json_encode($response);
}

?>