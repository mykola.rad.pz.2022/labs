<?php
function validateData(&$response, $data) {
    try {
        if (in_array(null, $data)) {
            $response = ['message' => 'All fields should be present'];
        } else if (!validateBirthday($data['birthday'])) {
            $response = ['message' => 'Invalid birthday format or age! Age must be between 16 and 60 years old!'];
        } else if (!validateName($data['lastName'])) {
          $response = ['message' => 'Invalid last name! First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed! Length should be more than 3 and less than 46 symbols!'];
        } else if (!validateName($data['firstName'])) {
          $response = ['message' => 'Invalid first name! First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed!Length should be more than 3 and less than 46 symbols!'];
        }else {
            $response = ['message' => $data];
            return true;
        }
   } catch (Exception $e) {
        error_log($e);
        $response = ['message' => 'Something went wrong'];
   }
    return false;
}

function validatePassword($password, $confirmPassword) {
  if (strlen($password) < 8 || strlen($password) > 16) {
      return false;
  }

  if (!preg_match('/[0-9]/', $password) || !preg_match('/[A-Z]/', $password)) {
      return false;
  }

  if ($password !== $confirmPassword) {
      return false;
  }

  return true;
}

function validateName($name) {
  $nameValue = trim($name);
  $namePattern = '/^[A-ZА-ЯҐЄІЇ][a-zа-яґєії\'-]*$/ui';

  if (!preg_match($namePattern, $nameValue)) 
    return false;

  if (strlen($nameValue) > 45 || strlen($nameValue) < 3) 
    return false;

  return true;
}

function validateUserName($name) {
  $nameValue = trim($name);
  $namePattern = '/^[A-ZА-ЯҐЄІЇa-zа-яґєії\'-]+$/ui';

  if (!preg_match($namePattern, $nameValue)) 
    return false;

    if (strlen($nameValue) > 50 || strlen($nameValue) < 3) 
    return false;
  
  return true;
}

function validateBirthday($birthday) {
    $age = (new DateTime())->diff(new DateTime($birthday))->y;
    return ($age >= 16 && $age <= 70);
}
?>