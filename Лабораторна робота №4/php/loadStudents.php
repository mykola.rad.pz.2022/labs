<?php
include "./db.php";

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type"); 

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  http_response_code(200);
  error_log("GET ALL USERS");
  $response = LoadStudentsFromDB();
  error_log(json_encode($response));
  echo json_encode($response);
} else {
  http_response_code(405);
  $response = ['message' => 'Invalid request method.'];
  error_log($response['message']);
  echo json_encode($response);
}
?>