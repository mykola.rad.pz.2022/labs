<?php
include './db.php';
include './studentValidation.php';

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");

session_start();
$_SESSION['userName'] = "Not registered";

function validateLogIn(&$response, $data) {
  try {
    if (in_array(null, $data)) {
        $response = ['message' => 'All fields should be present'];
    } else if (!validateUserName($data['userName'])) {
      $response = ['message' => 'Invalid user name! No spaces are allowed!Length should be more than 3 and less than 51 symbols!'];
    } else if (!checkPassword($data['userName'], $data['password'])) {
        $response = ['message' => 'Invalid password or username!'];
    } else {
      $_SESSION['userName'] = $data['userName'];
        $response = ['message' => $data];
        return true;
    }
} catch (Exception $e) {
    error_log($e);
    $response = ['message' => 'Something went wrong'];
}
return false;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  http_response_code(200);
  $jsonData = json_decode(file_get_contents('php://input'), true);

  $data = [
      'userName'     => isset($jsonData['userName']) ? $jsonData['userName'] : null,
      'password'   => isset($jsonData['password']) ? $jsonData['password'] : null,
  ];

foreach ($data as $key => $value) {
  error_log("$key: $value");
}

  $response = [];

  if(!validateLogIn($response, $data)) {
  http_response_code(400);
  }
  $response = ['userName' => $_SESSION['userName']];
  echo json_encode($response);
} else {
  http_response_code(405);
  $response = ['message' => 'Invalid request method.'];
  echo json_encode($response);
}
?>