<?php
$servername = ("localhost");
$username = "root";
$password = "TiGeR1789";
$dbname = "lab4db";

try {
    $conn = mysqli_connect($servername, $username, $password, $dbname);

} catch (mysqli_sql_exception) {
    error_log("Connection failed!");
}

function SaveStudentToDB(&$studentData) {

  global $conn;

  $id = $studentData['id'];
  $group = $studentData['group'];
  $firstName = $studentData['firstName'];
  $lastName = $studentData['lastName'];
  $gender = $studentData['gender'];
  $birthday = $studentData['birthday'];

  try {
    if ($id == -1) {

      $sql = "INSERT INTO students (st_group, firstName, lastName, gender, birthday)
      VALUES ('$group', '$firstName', '$lastName', '$gender', '$birthday');";
      
    mysqli_query($conn, $sql);
    $studentData['id'] = $conn->insert_id;

    } else {

      $sql = "UPDATE students SET st_group = '$group' , firstName = '$firstName', lastName = '$lastName', gender ='$gender', birthday = '$birthday' WHERE id = '$id'";
               
      mysqli_query($conn, $sql);
    }

    mysqli_close($conn);
    return true;
  }
  catch (mysqli_sql_exception) {
    error_log("Could not register user!");
    mysqli_close($conn);
    return false;
  }
}

function checkPassword($userName, $password)
{
  global $conn;

    $sql = "SELECT userPassword FROM users WHERE userName = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("s", $userName);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $dbPassword = $row['userPassword'];

        if ($password == $dbPassword) {
            return true; 
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function AddUserToDB(&$userData) {

  global $conn;

  $firstName = $userData['firstName'];
  $lastName = $userData['lastName'];
  $birthday = $userData['birthday'];
  $userName = $userData['userName'];
  $password = $userData['password'];

  try {
    $sql = "SELECT * FROM students WHERE firstName = ? AND lastName = ? AND birthday = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sss", $firstName, $lastName, $birthday);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        $studentData = $result->fetch_assoc();
        $studentID = $studentData['id'];

        $sql = "INSERT INTO users (id, userName, userPassword) VALUES (?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("iss", $studentID, $userName, $password);
        $stmt->execute();

        return true;
    } else {
        return false;
    }
} catch (Exception $e) {
    error_log("Could not register user: " . $e->getMessage());
    return false;
}
}

function LoadStudentsFromDB() {
  global $conn;
  $sql = "SELECT * FROM students";
  $result = mysqli_query($conn, $sql);
  $response = [];
  if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)) {
        $response[] = json_encode($row);
      }
  }
  return $response;
}

function DeleteStudentFromDB($id)
{
  global $conn;
  try {
      $sql = "DELETE FROM students WHERE id = '$id'";
      mysqli_query($conn, $sql);
      mysqli_close($conn);
      return true;
  } catch (mysqli_sql_exception) {
    error_log("Could not register user!");
    mysqli_close($conn);
    return false;
  }
}
?>