<?php
include './db.php';
include './studentValidation.php';

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");

function validateRegistration(&$response, $data) {
  try {
    if (in_array(null, $data)) {
        $response = ['message' => 'All fields should be present'];
    } else if (!validatePassword($data['password'], $data['confirmPassword'])) {
        $response = ['message' => 'Invalid password! Password shold contain at least 8 and no more than 16 symbols, 1 capital letter and 1 number! Also it has to be identical with confirmation password'];
    } else if (!validateName($data['lastName'])) {
      $response = ['message' => 'Invalid last name! First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed!Length should be more than 3 and less than 46 symbols!'];
    } else if (!validateName($data['firstName'])) {
      $response = ['message' => 'Invalid first name! First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed!Length should be more than 3 and less than 46 symbols!'];
    } else if (!validateUserName($data['userName'])) {
        $response = ['message' => 'Invalid user name! No spaces are allowed!Length should be more than 3 and less than 51 symbols!'];
    } else if (!validateBirthday($data['birthday'])) {
      $response = ['message' => 'Invalid birthday format or age! Age must be between 16 and 60 years old!'];
    }else {
        $response = ['message' => $data];
        return true;
    }
} catch (Exception $e) {
    error_log($e);
    $response = ['message' => 'Something went wrong'];
}
return false;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  http_response_code(200);
  $jsonData = json_decode(file_get_contents('php://input'), true);

  $data = [
      'firstName'    => isset($jsonData['firstName']) ? $jsonData['firstName'] : null,
      'lastName'     => isset($jsonData['lastName']) ? $jsonData['lastName'] : null,
      'birthday'     => isset($jsonData['birthday']) ? $jsonData['birthday'] : null,
      'userName'     => isset($jsonData['userName']) ? $jsonData['userName'] : null,
      'password'   => isset($jsonData['password']) ? $jsonData['password'] : null,
      'confirmPassword'     => isset($jsonData['confirmPassword']) ? $jsonData['confirmPassword'] : null,
  ];

foreach ($data as $key => $value) {
  error_log("$key: $value");
}

  $response = [];

  if(!validateRegistration($response, $data)) {
  http_response_code(400);
  } else if(!AddUserToDB($data)) {
    http_response_code(400);
    $response = ['message' => 'Something went wrong... Check your data! If everything is ok try to change login it has to be unique. If it will not help, ask administrator to add you to the system'];
  }

  echo json_encode($response);
} else {
  http_response_code(405);
  $response = ['message' => 'Invalid request method.'];
  echo json_encode($response);
}
?>