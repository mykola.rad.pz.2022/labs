<?php
include './db.php';
include './studentValidation.php';

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type"); 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    http_response_code(200);
    $jsonData = json_decode(file_get_contents('php://input'), true);

    $data = [
        'id'       => isset($jsonData['id']) ? $jsonData['id'] : -1,
        'group'    => isset($jsonData['group']) ? $jsonData['group'] : null,
        'firstName'    => isset($jsonData['firstName']) ? $jsonData['firstName'] : null,
        'lastName'     => isset($jsonData['lastName']) ? $jsonData['lastName'] : null,
        'gender'   => isset($jsonData['gender']) ? $jsonData['gender'] : null,
        'birthday' => isset($jsonData['birthday']) ? $jsonData['birthday'] : null
    ];

foreach ($data as $key => $value) {
    error_log("$key: $value");
}

    $response = [];

    if(!validateData($response, $data)) {
    http_response_code(400);
    }
    else if(!SaveStudentToDB($data)) {
        http_response_code(501);
        $response = ['message' => 'Unable to perform the operation'];
    } else {
        error_log("User saved successfully");
         $response = ['message' => $data];
    }
    echo json_encode($response);
} else {
    http_response_code(405);
    $response = ['message' => 'Invalid request method.'];
    echo json_encode($response);
}
?>