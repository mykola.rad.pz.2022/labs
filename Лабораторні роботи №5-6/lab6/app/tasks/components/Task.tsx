import { useCreateTask } from "@/app/contexts/CreateTaskContext";
import React, { useCallback } from "react";

type Props = {
  task: Task;
};

const Task = ({ task }: Props) => {
  const { openModal, setTask, setIsEdit } = useCreateTask();
  const onClick = useCallback(() => {
    setTask(task);
    setIsEdit(true);
    openModal();
  }, [task]);

  const showDeadline = useCallback(
    (e: React.MouseEvent<HTMLHeadingElement, MouseEvent>) => {
      e.currentTarget.innerText = task.deadline.split("-").reverse().join(".");
    },
    [task]
  );

  const hideDeadline = useCallback(
    (e: React.MouseEvent<HTMLHeadingElement, MouseEvent>) => {
      e.currentTarget.innerText = task.name;
    },
    [task]
  );

  const onDragStart = useCallback(
    (e: React.DragEvent<HTMLDivElement>) => {
      e.dataTransfer.setData("task", JSON.stringify(task));
      e.target.classList.add("opacity-50", "bg-secondary");
    },
    [task]
  );

  const onDragEnd = useCallback((e: React.DragEvent<HTMLDivElement>) => {
    e.target.classList.remove("opacity-50", "bg-secondary");
  }, []);


  return (
    <div onClick={onClick} onDragStart={onDragStart} onDragEnd={onDragEnd} draggable={true}>
      <div className="card my-2 p-1 bg-transparent   ">
        <div className="card-body row">
          <h5
            onMouseEnter={showDeadline}
            onMouseOut={hideDeadline}
            className="card-title"
          >
            {task.name}
          </h5>
        </div>
      </div>
    </div>
  );
};

export default Task;
