import { useCreateTask } from "@/app/contexts/CreateTaskContext";
import React, { useCallback, useEffect } from "react";
import Task from "./Task";
import { useTasks } from "@/app/contexts/TasksContext";
import { toast } from "react-toastify";

type Props = {
  stage: string;
  tasks: Task[];
};

const Board = ({ stage, tasks }: Props) => {
  const { openModal, setTask } = useCreateTask();
  const { updateTask } = useTasks();

  const dragOver = useCallback((e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
  }, []);
  const dragLeave = useCallback((e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
  }, []);
  const cancelDefault = useCallback((e: React.DragEvent<HTMLDivElement>) => {
    e.preventDefault();
  }, []);
  const dropped = useCallback(
    (e: React.DragEvent<HTMLDivElement>) => {
      e.preventDefault();
      const task = JSON.parse(e.dataTransfer.getData("task"));
      update({ ...task, stage: stage });
    },
    [stage]
  );

  return (
    <div className=" col-md-4 col-10  left-scroll p-2">
      <div
        className="border border-black mx-1"
        onDrop={dropped}
        onDragEnter={cancelDefault}
        onDragOver={dragOver}
        onDragLeave={dragLeave}
      >
        <div className="container-fluid">
          <h2 className="text-center  fs-4 py-1">{stage}</h2>
        </div>
        <div
          className="container-fluid left-scroll overflow-y-auto "
          style={{
            maxHeight: "400px",
          }}
        >
          {tasks.map((task, index) => (
            <Task key={index} task={task} />
          ))}
        </div>
        <div className="container-fluid fw-bolder fs-6  ">
          <button
            className="btn btn-outline-dark p-2 w-100 h-100 my-1 "
            type="button"
            aria-label="Create"
            onClick={() => {
              setTask({ stage: stage, deadline: "", name: "" } as Task);
              openModal();
            }}
          >
            +
          </button>
        </div>
      </div>
    </div>
  );

  function update({ _id, name, deadline, description, stage }: Task) {
    fetch(`${process.env.SERVER_URL}tasks`, {
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
      method: "PUT",
      body: JSON.stringify({
        id: _id,
        task: { name, deadline, description, stage },
      }),
    })
      .then((res) => {
        //console.log(res);
        if (res.status !== 200) throw res;
        return res.json();
      })
      .then((taskRes) => {
        toast.success("Success", {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
        updateTask(taskRes);
      })
      .catch(async (res) => {
        const err = await res.json();
        toast.error(err.message, {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
      });
  }
};

export default Board;
