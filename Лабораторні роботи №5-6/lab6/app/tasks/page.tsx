"use client";
import { useEffect } from "react";
import { useAuth } from "../contexts/AuthContext";
import { useRouter } from "next/navigation";
import { useTasks } from "../contexts/TasksContext";
import Board from "./components/Board";
import { useNotification } from "../contexts/NotificationContext";

type Props = {};

const Page = (props: Props) => {
  const { setStages, setTasks, stages, tasks } = useTasks();
  const { I } = useAuth();
  const router = useRouter();
  const { setTasks: setNotification, tasks: notification } = useNotification();

  useEffect(() => {
    process.env.SERVER_URL = "http://localhost:8080/";
    const fetchTasks = async () => {
      const jsonData: Response = await fetch(`${process.env.SERVER_URL}tasks`, {
        method: "GET",
        headers: { "Content-Type": "application/json" },
        credentials: "include",
      });
      //console.log(jsonData);
      const data: { stages: string[]; tasks: Task[] } = await jsonData.json();
      if (jsonData.status == 200) {
        setTasks(data.tasks);
        setStages(data.stages);
      } else {
        //@ts-ignore
        console.error("Error fetching data:", data.message);
      }
    };

    if (I) {
      fetchTasks();
    }
  }, [I]);

  useEffect(() => {
    if (notification.length != 0) setNotification([]);
  }, [notification]);

  //TODO: add in production
  // if (!I) {
  //   router.push("/login");
  //   return null;
  // }

  return (
    <main className="container-fluid  mt-4 ">
      <div className="row">
        <h1 className="float-lg-start ">Tasks</h1>
      </div>
      <div className="row  p-0 m-0 justify-content-center">
        {I &&
          stages.length != 0 &&
          stages.map((stage, index) => (
            <Board
              key={index}
              stage={stage}
              tasks={tasks.filter((task) => task.stage == stage)}
            />
          ))}
      </div>
    </main>
  );
};

export default Page;
