"use client";
import React, { useCallback, useEffect, useState } from "react";
import { Modal } from "./Modal";
import { toast } from "react-toastify";
import { useCreateChat } from "../contexts/CreateChatContext";
import { useSearchDebounce } from "@/hooks/useSearchDebounce";
import { useAuth } from "../contexts/AuthContext";
import { useChats } from "../contexts/ChatContext";
import { useRouter } from "next/navigation";
import UserIcon from "../components/UserIcon";
interface Props {
  // isOpen: boolean;
  // title: string;
  // student?: Student;
  // setStudent: Dispatch<SetStateAction<Student | undefined>>;
}

const CreateChatModal = ({}: Props) => {
  const {
    isOpen,
    students,
    closeModal,
    addStudent,
    removeStudent,
    setName,
    name,
    setStudents,
    isEditing,
    setIsEdit,
    chatId,
    setChatId,
  } = useCreateChat();

  const [search, setSearchQuery] = useSearchDebounce();
  const [searchStudents, setSearchStudents] = useState<User[]>([]);
  const { I } = useAuth();
  const { addChat } = useChats();

  useEffect(() => {
    if (!search) return;
    const getStudents = async () => {
      try {
        const response = await fetch(
          `${process.env.SERVER_URL}students/search?name=${search}`,
          {
            credentials: "include",
          }
        );
        const data = await response.json();
        //console.log("data from",data, searchStudents);
        if (response.status !== 200) throw new Error(data.message);
        setSearchStudents((prev) => data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    getStudents();
  }, [search]);

  const onChangeName = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setName(e.target.value);
    },
    [setName]
  );

  const onChangeSearch = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      //@ts-ignore
      setSearchQuery(e.target.value);
    },
    [setSearchQuery]
  );

  const onCreateChat = () => {
    const arr = students.map((student) => student._id);
    arr.push(I!._id);
    // console.log("Create chat", {
    //   name: name,
    //   students: arr,
    //   messages: [],
    // });
    createRequest(
      fetch(`${process.env.SERVER_URL}chats`, {
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        method: "POST",
        body: JSON.stringify({
          name: name,
          users: arr,
          messages: [],
        }),
      })
    );
    removeStudent(I as User);
  };

  const onEditChat = () => {
    const arr = students.map((student) => student._id);
    createRequest(
      fetch(`${process.env.SERVER_URL}chats`, {
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        method: "PUT",
        body: JSON.stringify({
          _id: chatId,
          users: arr,
        }),
      })
    );
    removeStudent(I as User);
  };

  const onClose = useCallback(() => {
    if (isEditing) {
      setIsEdit(false);
      setName("");
      setStudents([]);
      setChatId("");
    }
    closeModal();
  }, [isEditing]);

  return (
    <Modal
      isOpen={isOpen}
      useHeader={true}
      onClose={onClose}
      header={
        <div className="container-fluid ">
          <h2 className="fw-bolder fs-2">
            {isEditing ? "Edit chat" : "Create chat"}
          </h2>
          {!isEditing && (
            <input
              type="text"
              className="w-100 rounded-3 border border-secondary p-2 mt-3"
              placeholder="Chat name"
              value={name}
              onChange={onChangeName}
            />
          )}
          <div
            className=" left-scroll overflow-x-scroll mt-3 p-0  "
            style={{ maxHeight: "20vh" }}
          >
            {students.map((student) => (
              <GetUserRowView
                key={student._id}
                student={student}
                onClick={() => removeStudent(student)}
              />
            ))}
          </div>
        </div>
      }
      body={
        <div className="container-fluid ">
          <h2 className="fs-2">Add partisipants</h2>
          <input
            type="text"
            className="w-100 rounded-3 border border-secondary p-2 mt-3"
            placeholder="Search participants"
            onChange={onChangeSearch}
          />
          <div
            className="left-scroll overflow-x-scroll mt-3 p-0"
            style={{ maxHeight: "50vh" }}
          >
            {searchStudents
              .filter((st) => !students.find((s) => st._id == s._id))
              .map((student) => (
                <GetUserRowView
                  key={student._id}
                  student={student}
                  onClick={() => addStudent(student)}
                />
              ))}
          </div>
        </div>
      }
      footer={
        <div className="container-fluid fw-bolder fs-6  ">
          {!isEditing ? (
            <button
              className="btn btn-outline-dark p-2 w-100 h-100"
              type="button"
              aria-label="Create"
              onClick={onCreateChat}
            >
              Create
            </button>
          ) : (
            <button
              className="btn btn-outline-dark p-2 w-100 h-100"
              type="button"
              aria-label="Edit"
              onClick={onEditChat}
            >
              Edit
            </button>
          )}
        </div>
      }
    />
  );

  async function createRequest(promise: Promise<Response>) {
    promise
      .then((res) => {
        //console.log(res);
        if (res.status !== 200) throw res;
        return res.json();
      })
      .then((chat) => {
        toast.success("Success", {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
        if (!isEditing) addChat(chat);
        else window.location.reload();
        setName("");
        setStudents([]);
        closeModal();
      })
      .catch(async (res) => {
        const err = await res.json();
        toast.error(err.message, {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
      });
  }
};

interface RowViewProps {
  student: User;
  onClick: () => void;
}

function GetUserRowView({ student, onClick }: RowViewProps) {
  return (
    <div className="" onClick={onClick}>
      <div className="d-flex  gap-2">
        <UserIcon url={student.avatar || "/user.jpg"} />
        <p className="fs-3">{student.firstName + " " + student.lastName}</p>
      </div>
    </div>
  );
}

export default CreateChatModal;
