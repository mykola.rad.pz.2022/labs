"use client";
import { use, useEffect, useState } from "react";
import Chats from "./components/Chats";
import Dialog from "./components/Dialog";
import { useChats } from "../contexts/ChatContext";
import { useSearchParams } from "next/navigation";
import { useAuth } from "../contexts/AuthContext";
import { useRouter } from "next/navigation";

type Props = {};

const Page = (props: Props) => {
  const searchParams = useSearchParams();
  const chatId = searchParams.get("chat") || "";
  const { I, chats, setChats } = useChats();
  const router = useRouter();

  

  useEffect(() => {
    process.env.SERVER_URL = "http://localhost:8080/";
    const fetchChats = async () => {
      const jsonData: Response = await fetch(
        `${process.env.SERVER_URL}chats/${I?._id}`,
        {
          method: "GET",
          headers: { "Content-Type": "application/json" },
          credentials: "include",
        }
      );
      //console.log(jsonData);
      const data = await jsonData.json();
       console.log("fetch chats from server",data);
      if (jsonData.status === 200 && data.length > 0) {
        setChats(data.map((chat: Partial<Chat>) => ({ ...chat, messages: [] })));
      } else {
        console.error("Error fetching data:", data.message);
      }
    };
    
    if (I && chats?.length === 0) {
     // console.log("I", I, chats, "fetching chats");
      fetchChats();
    }
  }, [I, chats]);

  //TODO: add in production
  // if (!I) {
  //   router.push("/login");
  //   return null;
  // }

  return (
    <main className="container-fluid  mt-4 ">
      <div className="row">
        <h1 className="float-lg-start ">Messages</h1>
      </div>

      <div className="row p-0 m-0 gap-2 ">
        {I && chats && (
          <>
            <Chats  chats={chats} currentChatId={chatId} />
            <Dialog chat={chats.find((ch) => ch._id == chatId)} />
          </>
        )}
      </div>
    </main>
  );
};

export default Page;
