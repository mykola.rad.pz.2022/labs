import React, { useCallback, useEffect, useRef, useState } from "react";
import MessageItem from "./MessageItem";
import ChatItem from "./ChatItem";
import { useChats } from "@/app/contexts/ChatContext";
import { useSocket } from "@/app/contexts/SocketContext";
import { useCreateChat } from "@/app/contexts/CreateChatContext";

type Props = {
  chat: Chat | undefined;
};

const Dialog = ({ chat }: Props) => {
  //console.log("chat", chat);
  const [message, setMessage] = useState("");
  const { I, sendMessage, downloadMessages, setUnreadMessages } = useChats();
  const messagesEndRef = useRef(null);
  const { setChat } = useSocket();
  const { openModal, setStudents, setIsEdit, setChatId } = useCreateChat();

  //console.log("chat in dialog", chat);

  const scrollToBottom = () => {
    if (messagesEndRef.current) {
      // @ts-ignore
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  useEffect(() => {
    if (
      chat &&
      (chat.messages.length == 0 ||
        chat.messages.length ==
          I?.unreadMessages?.find((unread) => unread.chatId == chat._id)
            ?.unread)
    ) {
      //console.log("downloading messages", chat, downloadMessages);
      downloadMessages(chat._id, true);
    }
    if (chat && chat.messages.length > 0) setUnreadMessages(chat._id, 0);

    setChat(chat);
    console.log("set chat in dialog", chat);
  }, [chat]);

  useEffect(() => {
    scrollToBottom();
  }, [chat?.messages.length]);

  const onMessageChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setMessage(e.target.value);
    },
    []
  );

  const onSend = useCallback(() => {
    if (!message) return;
    sendMessage(createMessage());
    setMessage("");
  }, [message, chat, I]);

  const onClickEditChat = useCallback(() => {
    setIsEdit(true);
    setStudents(chat?.users || []);
    setChatId(chat?._id || "");
    openModal();
  }, [chat]);

  if (!chat)
    return (
      <section
        className=" border border-secondary mb-3 overflow-y-auto without-scroll rounded col-7 flex-grow-1 align-content-center"
        style={{ height: "75vh" }}
      >
        <div className="p-0 container-fluid">
          <h3 className="text-center align-content-center ">
            Виберіть чат для спілкування
          </h3>
        </div>
      </section>
    );

  return (
    <section
      className="position-relative border border-secondary mb-3 rounded col-7 flex-grow-1"
      style={{ height: "75vh" }}
    >
      <h2
        className="text-center sticky-top bg-white p-1 z-3"
        style={{ cursor: "pointer" }}
        onClick={onClickEditChat}
      >
        {chat.name}
      </h2>
      <div
        className=" overflow-y-auto without-scroll "
        style={{ height: "60vh" }}
      >
        <div className="p-0 container-fluid">
          {
            <>
              {chat.messages
                ?.map((message, i) => (
                  <MessageItem
                    message={message}
                    authorName={
                      typeof message.author === "string"
                        ? getUserName(
                            chat.users.find((u) => u._id == message.author)!
                          )
                        : getUserName(message.author)
                    }
                    isLeft={
                      (typeof message.author === "string"
                        ? message.author
                        : message.author._id) != I!._id
                    }
                    key={i}
                  />
                ))
                .reverse()}
              <div ref={messagesEndRef} />
            </>
          }
        </div>
      </div>
      <div className="container-fluid position-absolute bottom-0 start-0 bg-white mb-2 p-1 z-3">
        <div className="row align-items-center m-0 ">
          <div className="form-floating col-10 m-0">
            <input
              type="text"
              className="form-control"
              name="msg"
              id="msg"
              placeholder="Type your message"
              value={message}
              onChange={onMessageChange}
            />
            <label htmlFor="msg" className="ms-2">
              Type your message
            </label>
          </div>
          <div className="form-floating col-2 m-0 ">
            <button
              type="button"
              className="p-3 btn btn-outline-dark "
              aria-label="Send"
              onClick={onSend}
            >
              Send
            </button>
          </div>
        </div>
      </div>
    </section>
  );

  function createMessage(): Message {
    return {
      _id: "",
      author: I!._id,
      chatId: chat!._id,
      text: message,
      createdAt: new Date().toISOString(),
    };
  }

  function getUserName(user: User): string {
    console.log(user);
    return `${user.firstName} ${user.lastName}`;
  }
};

export default Dialog;
