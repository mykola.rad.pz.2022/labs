import React from "react";
import ChatItem from "./ChatItem";
import { calculateSizeAdjustValues } from "next/dist/server/font-utils";
import { inherits } from "util";
import { useCreateChat } from "@/app/contexts/CreateChatContext";
import { useRouter } from "next/navigation";
import { useNotification } from "@/app/contexts/NotificationContext";
import { useAuth } from "@/app/contexts/AuthContext";

type Props = {
  currentChatId: string;
  chats: Chat[];
};

const Chats = ({ chats, currentChatId }: Props) => {
  const { I } = useAuth();
  const { openModal } = useCreateChat();
  const router = useRouter();
  //console.log("Cahts and uread msgs", messages);
  //console.log("Chats i and unread", I);

  return (
    <section
      className=" border border-secondary p-1 rounded col-sm-4 col-12 h-100  overflow-auto without-scroll "
      style={{ maxHeight: "75vh" }}
    >
      <div className="accordion" id="chatsAccording">
        <div className="accordion-item">
          <div className="accordion-header d-flex flex-row gap-1 z-1 ">
            <button
              className="accordion-button p-2"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#collapseOne"
              aria-expanded="true"
              aria-controls="collapseOne"
            >
              <p className=" ms-1 m-0 fw-bold">Chat room</p>
            </button>
            <button className="btn btn-outline-dark" onClick={openModal}>
              +
            </button>
          </div>
          <div
            id="collapseOne"
            className="accordion-collapse collapse show "
            data-bs-parent="#chatsAccording"
          >
            <div className="accordion-body p-0">
              <ol className="list-group m-0 ">
                {chats.map((chat, i) => (
                  <ChatItem
                    active={currentChatId == chat._id}
                    id={chat._id}
                    lastMessage={
                     chat.messages[0] ||
                      ({
                        text: "",
                      } as Message)
                    }
                    name={chat.name}
                    unread={
                      I?.unreadMessages?.find((um) => um.chatId == chat._id)
                        ?.unread || 0
                    }
                    key={chat._id}
                    onClick={() => {
                      router.push(`/messages?chat=${chat._id}`);
                    }}
                  />
                ))}
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Chats;
