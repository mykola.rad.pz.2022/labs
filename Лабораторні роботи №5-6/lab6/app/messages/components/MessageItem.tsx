import React from "react";

type Props = {
  message: Message;
  authorName: string;
  isLeft: boolean;
};

const MessageItem = ({ message, isLeft, authorName }: Props) => {
  return (
    <div
      className={
        "row " +
        (isLeft
          ? " justify-content-start text-start  "
          : " justify-content-end text-end ")
      }
    >
      <div className=" mt-2 align-baseline col-6 col-md-11 ">
        <div
          className="rounded bg-secondary-subtle d-inline-block   p-3 pb-0"
          style={{ overflowWrap: "anywhere" }}
        >
          <h6 className="text-secondary fw-bold" style={{ fontSize: "0.7rem" }}>
            
            {authorName + " :"}
          </h6>
          <p className="text-wrap fs-5 ">{message.text}</p>
          <h6
            className={
              "text-secondary   " + (isLeft ? " text-end  " : " text-start ")
            }
            style={{ fontSize: "0.7rem" }}
          >
            {new Date(message.createdAt)
              .toUTCString()
              .split(",")[1]
              .replace("GMT", "")}
          </h6>
        </div>
      </div>
    </div>
  );
};

export default MessageItem;
