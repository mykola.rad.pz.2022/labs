"use client";
import {
  ReactNode,
  createContext,
  use,
  useCallback,
  useContext,
  useState,
} from "react";
import { useAuth } from "./AuthContext";

interface ChatsContextType {
  I: User | undefined;
  chats: Chat[];
  addChat: (chat: Chat) => void;
  sendMessage: (message: Message) => void;
  setChats: (chats: Chat[]) => void;
  reciveMessages: (message: Message[]) => void;
  downloadMessages: (chatId: string, clear: boolean) => void;
  setUnreadMessages: (chatId: string, unread: number) => void;
  addMessageToChat: (message: Message, chat: Chat) => void;
}

const ChatContext = createContext<ChatsContextType>({
  I: undefined,
  chats: [],
  addChat: function (chat: Chat): void {},
  sendMessage: function (message: Message): void {},
  setChats: function (chats: Chat[]): void {},
  reciveMessages: function (message: Message[]): void {},
  downloadMessages: function (chatId: string, clear: boolean): void {},
  setUnreadMessages: function (chatId: string, unread: number): void {},
  addMessageToChat: function (message: Message, chat: Chat): void {},
});

export const useChats = () => useContext(ChatContext);

interface Props {
  children: ReactNode;
}

export const ChatsProvider = ({ children }: Props) => {
  const { I, role, setUser } = useAuth();

  const [chats, setChats] = useState<Chat[]>([]);
  const addChat = useCallback((chat: Chat) => {
    setChats((prev) => [...prev, chat]);
  }, []);

  const sendMessage = useCallback((message: Message) => {
    process.env.SERVER_URL = `http://localhost:8080/`;
    fetch(`${process.env.SERVER_URL}messages`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
      body: JSON.stringify(message),
    })
      .then((res) => {
        
        if (res.status !== 200) throw res;
        return res.json();
      })
      .then((message) => {
        setChats((prev) =>
          prev.map((c) =>
            c._id === message.chatId
              ? { ...c, messages: [message, ...c.messages] }
              : c
          )
        );
      })
      .catch((err) => {
        console.error("Error sending message", err);
      });
  }, []);

  const set = useCallback((chats: Chat[]) => {
    setChats(chats);
  }, []);

  const reciveMessages = useCallback(
    (messages: Message[]) => {
      if (!I) return;
      setChats((prev) => {
        let changes: Chat[] = [];
        let newUnread = [...(I?.unreadMessages || [])];
        const ids: string[] = prev.map((c) => c._id);
       // console.log("recive messages", messages, "unread messages", I.unreadMessages,"new unread arr", newUnread,"caht ids", ids);
        
        messages.forEach((m) => {
         let unreadIndex = newUnread.findIndex((u) => u.chatId == m.chatId);
         // console.log("unread index\n\n\n", unreadIndex);
          if (unreadIndex != -1) {
            newUnread[unreadIndex].unread++;
          } else {
            newUnread.push({ chatId: m.chatId, unread: 1 });
          }

          if (ids.find((id) => id == m.chatId)) {
            const chat = prev.find((c) => c._id == m.chatId)!;
            changes.push({ ...chat, messages: [m, ...chat.messages] });
          } else {
            prev.push({
              _id: m.chatId,
              //@ts-ignore
              name: m.chatName,
              messages: [m],
              //@ts-ignore
              users: m.users,
            });
          }
        });
        setUser({ ...I, unreadMessages: newUnread });
        return prev.map((c) => changes.find((ch) => ch._id == c._id) || c);
      });
    },
    [I]
  );

  const setUnreadMessages = useCallback(
    (chatId: string, unreadMessages: number) => {
      // console.log("set unread messages", chatId, unreadMessages, I);
      if (I) {
        setUser({
          ...I!,
          unreadMessages: I.unreadMessages
            ? I!.unreadMessages.map((unerad) => {
                if (chatId == unerad.chatId) {
                  unerad.unread = unreadMessages;
                }
                return unerad;
              })
            : [{ chatId: chatId, unread: unreadMessages }],
        });
        // console.log("set unread messages already", I);
      }
    },
    [I, setUser]
  );

  const downloadMessages = useCallback((chatId: string, clear: boolean = false) => {
    // console.log("Downloading messages");
    process.env.SERVER_URL = `http://localhost:8080/`;
    fetch(`${process.env.SERVER_URL}messages/${chatId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
    })
      .then((res) => {
        if (res.status !== 200) throw res;
        return res.json();
      })
      .then((messages: Message[]) => {
        if (messages.length === 0) return;
        setChats((prev) =>
          prev.map((c) =>
            c._id === chatId
              ? { ...c, messages: [...(clear ? [] : c.messages), ...messages] }
              : c
          )
        );
      })
      .catch((err) => {
        console.error("Error downloading messages", err);
      });
  }, []);


  const addMessageToChat = useCallback((message: Message, chat: Chat) => 
    {
      setChats((prev) =>
        prev.map((c) =>
          c._id == chat._id
            ? { ...c, messages: [message, ...c.messages] }
            : c
        )
      );
    }, []);

  return (
    <ChatContext.Provider
      value={{
        chats,
        addChat,
        sendMessage,
        setChats: set,
        reciveMessages,
        I,
        downloadMessages,
        setUnreadMessages,
        addMessageToChat,
      }}
    >
      {children}
    </ChatContext.Provider>
  );
};
