"use client";
import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useState,
} from "react";
import CreateChatModal from "../modals/CreateChatModal";

interface CreateChatContextType {
  chatId: string;
  setChatId: (chatId: string) => void;
  isEditing: boolean;
  students: User[];
  addStudent: (student: User) => void;
  removeStudent: (student: User) => void;
  name: string;
  setName: (name: string) => void;
  setStudents: (students: User[]) => void;
  isOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
  setIsEdit : (isEdit: boolean) => void;
}

const CreateChatContext = createContext<CreateChatContextType>({
  chatId: "",
  setChatId: (chatId: string) => {},
  isEditing: false,
  students: [],
  addStudent: (student: User) => {},
  removeStudent: (student: User) => {},
  name: "",
  setName: (name: string) => {},
  setStudents: (students: User[]) => {},
  isOpen: false,
  openModal: () => {},
  closeModal: () => {},
  setIsEdit : (isEdit: boolean) => {},
});

export const useCreateChat = () => useContext(CreateChatContext);

interface Props {
  children: ReactNode;
}
export const CreateChatProvider = ({ children }: Props) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [students, setStudent] = useState<User[]>([]);
  const [name, setNam] = useState<string>("");
  const [isOpen, setIsOpen] = useState(false);
  const [chatId, setChaId] = useState<string>("");
  
  const setChatId = useCallback((chatId: string) => {
    setChaId(chatId);
  }, []);

  const openModal = useCallback(() => {
    setIsOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsOpen(false);
  }, []);

  const setName = useCallback((name: string) => {
    setNam(name);
  }, []);

  const addStudent = useCallback((student: User) => {
    if (!students.find((s) => s._id === student._id)) {
      setStudent((prev) => [...prev, student]);
    }
    //console.log(students);
  }, [students]);

  const removeStudent = useCallback((student: User) => {
    setStudent((prev) => prev.filter((s) => s._id !== student._id));
  }, []);

  const setStudents = useCallback((students: User[]) => {
    setStudent((prev) => students);
  }, []);

  const setIsEdit = useCallback((isEdit: boolean) => {
    setIsEditing(isEdit);
  }, []);

  return (
    <CreateChatContext.Provider
      value={{
        students,
        addStudent,
        removeStudent,
        name,
        setName,
        setStudents,
        isOpen,
        openModal,
        closeModal,
        isEditing,
        setIsEdit,
        chatId,
        setChatId,
      }}
    >
      <CreateChatModal />
      {children}
    </CreateChatContext.Provider>
  );
};
