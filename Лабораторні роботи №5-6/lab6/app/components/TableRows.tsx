"use client";

import { useSearchParams } from "next/navigation";
import { useEffect, useState } from "react";
import { useStudent } from "../contexts/StudentsContext";

interface Props {
  onEdit: (student: User) => void;
  onDelete: (student: User[]) => void;
  selectAll: boolean;
}

function TableRows({ onEdit, onDelete, selectAll }: Props) {
  const searchParams = useSearchParams();
  const page = searchParams.get("page") || "1";
  const { students, setSudents } = useStudent();
  const [checked, setChecked] = useState<User[]>([]);

  useEffect(() => {
    setChecked(selectAll ? students : []);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectAll]);

  useEffect(() => {
    let isLoading = false;
    const fetchStudents = async () => {
      isLoading = true;
      process.env.SERVER_URL = "http://localhost:8080/";
      const studentsData: Response = await fetch(
        `${process.env.SERVER_URL}students?page=${page}`,
        {
          method: "GET",
          credentials: "include",
        }
      );
       
      let students = await studentsData.json();
      if(!students.length) {
        students = [students];
      }

       console.log(students);
      if (isLoading) {
        setSudents(students);
      }
    };

    fetchStudents();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page]);

  if (students.length === 0) {
    return (
      <tr>
        <td colSpan={7} className="text-center">
          Loading students...
        </td>
      </tr>
    );
  }

  function handleDelate(
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    student: User
  ): void {
    const chekedSet = new Set(checked);
    const commonStudents = students.filter((s) => chekedSet.has(s));
    if (commonStudents.length === 0) onDelete([student]);
    else onDelete(commonStudents);
    setChecked(commonStudents);
  }

  return students.map((student) => (
    <tr key={student._id}>
      <td>
        <input
          type={"checkbox"}
          className="delete-checkbox"
          aria-label="Delete checkbox"
          value={student._id}
          name="delete-checkbox"
          checked={checked.some((item) => item._id === student._id)}
          onChange={(e) => {
            if (e.target.checked) {
              setChecked((prev) => [...prev, student]);
            } else {
              setChecked((prev) =>
                prev.filter((item) => item._id !== student._id)
              );
            }
          }}
        />
      </td>
      <td>{student.group}</td>
      <td>
        <span>{student.firstName + " "}</span>

        <span>{student.lastName}</span>
      </td>
      <td>{student.gender}</td>
      <td>{student.birthday.split("-").reverse().join(".")}</td>
      <td>
        <span
          className={
            student.status
              ? "text-bg-success rounded-circle  d-inline-block"
              : "text-bg-secondary rounded-circle  d-inline-block"
          }
          style={{ width: "1.5rem", height: "1.5em" }}
        ></span>
      </td>
      <td>
        <button
          className="btn btn-outline-dark"
          aria-label="edit user"
          onClick={() => onEdit(student)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="align-baseline  inverse-color"
            height="1rem"
            viewBox="0 0 512 512"
          >
            <path
              strokeWidth="1"
              stroke="currentColor"
              style={{ fill: "currentColor" }}
              d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z"
            />
          </svg>
        </button>
        <button
          className="  btn btn-outline-dark"
          data-bs-toggle="modal"
          data-bs-target="#deleteUserModal"
          aria-label="delete user"
          onClick={(e) => handleDelate(e, student)}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="align-baseline inverse-color "
            height="1rem"
            viewBox="0 0 352 512"
          >
            <path
              strokeWidth="1"
              stroke="currentColor"
              style={{ fill: "currentColor" }}
              d="M242.7 256l100.1-100.1c12.3-12.3 12.3-32.2 0-44.5l-22.2-22.2c-12.3-12.3-32.2-12.3-44.5 0L176 189.3 75.9 89.2c-12.3-12.3-32.2-12.3-44.5 0L9.2 111.5c-12.3 12.3-12.3 32.2 0 44.5L109.3 256 9.2 356.1c-12.3 12.3-12.3 32.2 0 44.5l22.2 22.2c12.3 12.3 32.2 12.3 44.5 0L176 322.7l100.1 100.1c12.3 12.3 32.2 12.3 44.5 0l22.2-22.2c12.3-12.3 12.3-32.2 0-44.5L242.7 256z"
            />
          </svg>
        </button>
      </td>
    </tr>
  ));
}

export default TableRows;
