import Link from 'next/link';
import { useCallback } from 'react';
import { useAuth } from '../contexts/AuthContext';

type Props = {}

const SigninButtons = (props: Props) => {

  const {setUser, setRole, I} = useAuth();

  const handleLogout = useCallback(() => {
    // console.log("logout");
    fetch(`${process.env.SERVER_URL}logout`, {
      method: "GET",
      credentials: "include",
    }).then((res) => {
      if (res.status === 200) {
        setUser(undefined);
        setRole(undefined);
        window.location.href = "/";
      }
    }).catch((err) => {
      console.log(err);
    });

  }, []);


  return (
    <div className="profile-container-singin">
      <button className="dropdown-btn" id="dropdown-btn" aria-haspopup="menu">
        <span>{I?.firstName + " " + I?.lastName}</span>
        <span className="arrow"></span>
      </button>
      <ul className="dropdown-content" role="menu">
        <li style={{ "--delay": 1 } as any}>
          <Link href="/">Profile</Link>
        </li>
        <li style={{ "--delay": 2 } as any}>
          <Link href="/" onClick={handleLogout}>
            Log out
          </Link>
        </li>
      </ul>
    </div>
  );
}

export default SigninButtons