import Link from "next/link";

type Props = {
  path: string;
};

const Navbar = ({ path = "/" }: Props) => {
  return (
    <nav className="navbar">
      <div className="container-fluid">
        <button
          className="navbar-toggler btn-outline-secondary m-auto text-bg-light"
          type="button"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasNavbar"
          aria-controls="offcanvasNavbar"
          aria-label="Toggle navigation"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="align-baseline "
            height="1rem"
            viewBox="0 0 512 512"
          >
            <path d="M48 48a48 48 0 1 0 48 48 48 48 0 0 0 -48-48zm0 160a48 48 0 1 0 48 48 48 48 0 0 0 -48-48zm0 160a48 48 0 1 0 48 48 48 48 0 0 0 -48-48zm448 16H176a16 16 0 0 0 -16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0 -16-16zm0-320H176a16 16 0 0 0 -16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16V80a16 16 0 0 0 -16-16zm0 160H176a16 16 0 0 0 -16 16v32a16 16 0 0 0 16 16h320a16 16 0 0 0 16-16v-32a16 16 0 0 0 -16-16z" />
          </svg>
        </button>
        <Link
          className="navbar-brand m-auto fw-medium  text-white m-sm-2 d-none d-sm-block "
          href="/"
        >
          CMS
        </Link>
        <div
          className="offcanvas offcanvas-start"
          tabIndex={-1}
          id="offcanvasNavbar"
          aria-labelledby="offcanvasNavbarLabel"
        >
          <div className="offcanvas-header">
            <h4 className="offcanvas-title" id="offcanvasNavbarLabel">
              Navigation
            </h4>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>
          <div className="offcanvas-body">
            <ul className="navbar-nav navigation-ul">
              <li
                className={
                  path === "/dashboard" ? "nav-item fw-bold " : "nav-item "
                }
              >
                <Link className="nav-link link-dark" href="/dashboard">
                  Dashboard
                </Link>
              </li>
              <li className={path === "/" ? "nav-item fw-bold " : "nav-item "}>
                <Link className="nav-link link-dark" href="/">
                  Studens
                </Link>
              </li>
              <li
                className={
                  path === "/tasks" ? "nav-item fw-bold " : "nav-item "
                }
              >
                <Link className="nav-link link-dark" href="/tasks">
                  Task
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
