"use client";
import { use, useCallback, useEffect, useRef, useState } from "react";
import { useNotification } from "../contexts/NotificationContext";
import MessageItem from "./MessageItem";
import { useRouter } from "next/navigation";
import TaskItem from "./TaskItem";
import { useAuth } from "../contexts/AuthContext";

function Notification() {
  const btn = useRef<HTMLButtonElement>(null);
  const { I } = useAuth();
  const { messages, tasks, setTasks } = useNotification();
  const router = useRouter();
  const handleClick = useCallback(() => {
    router.push("/messages");
  }, [router]);
  const [notification, setNotification] = useState<JSX.Element[] | null>(null);
  const uread =
    (I?.unreadMessages?.reduce((acc, cur) => acc + cur.unread, 0) ?? 0) +
    tasks.length;

  const msg = () => {
    setNotification([
      ...tasks.map((task, i) => (
        <li
          key={task._id}
          style={{ "--delay": i + 1 } as any}
          onClick={() => {
            router.push("/tasks");
          }}
        >
          <TaskItem task={task} />
        </li>
      )),
      ...messages
        .filter((e) => e != undefined)
        .map((msg, i) => (
          <li
            key={msg._id}
            style={{ "--delay": i + 1 } as any}
            onClick={() => {
              router.push("/messages?chat=" + msg.chatId);
            }}
          >
            <MessageItem
              from={
                typeof msg.author === "string"
                  ? "unknown"
                  : `${msg.author.firstName} ${msg.author.lastName || ""}`
              }
              message={msg.text}
              handleClick={() => {
                router.push("/messages?chat" + msg.chatId);
              }}
            />
          </li>
        )),
    ]);
  };

  useEffect(() => {
    if (btn && btn.current) {
      btn.current.addEventListener("focus", msg);
      const focusOutHandler = () => {
        setTimeout(() => {
          setNotification(null);
        }, 500);
      };
      btn.current.addEventListener("focusout", focusOutHandler);

      return () => {
        btn.current?.removeEventListener("focus", msg);
        btn.current?.removeEventListener("focusout", focusOutHandler);
      };
    }
  });

  return (
    <div className="notification-container" onDoubleClick={handleClick}>
      {uread > 0 && (
        <span
          className="badge text-bg-primary rounded-pill"
          style={{ position: "absolute", top: "-0.7rem", right: "-0.7rem" }}
        >
          {uread}
        </span>
      )}

      <button
        className="notification"
        aria-haspopup="menu"
        aria-label="notification"
        ref={btn}
      >
        <svg
          className="w-6 h-6 text-gray-800 dark:text-white"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          width={"2rem"}
        >
          <path
            stroke="currentColor"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M12 5.4V3m0 2.4a5.3 5.3 0 0 1 5.1 5.3v1.8c0 2.4 1.9 3 1.9 4.2 0 .6 0 1.3-.5 1.3h-13c-.5 0-.5-.7-.5-1.3 0-1.2 1.9-1.8 1.9-4.2v-1.8A5.3 5.3 0 0 1 12 5.4ZM8.7 18c.1.9.3 1.5 1 2.1a3.5 3.5 0 0 0 4.6 0c.7-.6 1.3-1.2 1.4-2.1h-7Z"
          />
        </svg>
      </button>

      <ul className="dropdown-notification-content without-scroll" role="menu">
        {notification}
      </ul>
    </div>
  );
}

export default Notification;
