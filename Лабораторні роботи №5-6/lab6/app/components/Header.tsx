"use client";
import Link from "next/link";
import Navbar from "./Navbar";
import { usePathname } from "next/navigation";
import Notification from "./Notification";
import SigninButtons from "./SigninButtons";
import { useAuth } from "../contexts/AuthContext";
import UserIcon from "./UserIcon";

type Props = {};

const Header = (props: Props) => {
  const path = usePathname();
  const { I } = useAuth();

  if (!I) return null;

  return (
    <header className="w-100 sticky-top" style={{ zIndex: 99 }}>
      <div className="d-flex justify-content-between bg-secondary pe-3 align-items-center ">
        <Navbar path={path} />
        <div className="d-flex justify-content-between gap-2  align-items-center">
          <Notification />
          <Link href="/" className="profile-container" aria-label="profile">
            <UserIcon url={I?.avatar || "/user.jpg"} />
            {}
          </Link>
          <SigninButtons />
        </div>
      </div>
    </header>
  );
};

export default Header;
