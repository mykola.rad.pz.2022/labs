"use client";

import { useState } from "react";
import { Modal } from "../modals/Modal";
import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
import { useAuth } from "../contexts/AuthContext";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import UserIcon from "../components/UserIcon";

type Props = {};

let photoIndex = 0;
const photoUrls = ["/user.jpg", "/user1.jpg", "/user2.jpg", "/user3.jpg"];

const settings = {
  className: "center",
  centerMode: true,
  infinite: true,
  centerPadding: "60px",
  slidesToShow: 3,
  swipeToSlide: true,
  dots: true,
  speed: 500,
  focusOnSelect: true,
  afterChange: function (index: number) {
    photoIndex = index;
  },
};

const Page = (props: Props) => {
  const [isSaving, setIsSaving] = useState<boolean>(false);
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const router = useRouter();
  const { I, setUser, setRole } = useAuth();

  if (I) {
    router.push("/");
    return null;
  }

  const auth = async () => {
    const form = document.getElementById("auth-form") as HTMLFormElement;
    if (form.checkValidity() && !isSaving) {
      setIsSaving(true);
      const data = getDataFromForm(new FormData(form));
      //console.log(data);
      process.env.SERVER_URL = "http://localhost:8080/";
      await sendRequest(
        fetch(`${process.env.SERVER_URL}login`, {
          headers: {
            "Content-Type": "application/json",
          },
          credentials: "include",
          method: "POST",
          body: JSON.stringify(data),
        })
      );
      setIsSaving(false);
    }
    form.classList.add("was-validated");
  };

  return (
    <Modal
      useHeader={true}
      isOpen={true}
      header={
        <div className="container-fluid text-center justify-content-center fw-bold fs-6">
          Login
        </div>
      }
      body={
        <form
          id="auth-form"
          className="bg-white rounded  needs-validation"
          noValidate
        >
          <div className="container-fluid">
            <div className="form-floating mb-3 ">
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                placeholder="Full name"
                pattern="[A-ZА-ЯІЄЇ][a-z'а-яїіє]{1,} [A-ZА-ЯІЄЇ][a-z'а-яїіє]{1,}"
                required
              />
              <label htmlFor="name">Full name</label>
              <div className="invalid-feedback">Pleas, enter full name</div>
            </div>

            <div className="input-group mb-3 ">
              <div className="form-floating ">
                <input
                  type={showPassword ? "text" : "password"}
                  className="form-control"
                  id="password"
                  name="password"
                  placeholder="Password"
                  required
                />
                <label htmlFor="password">Password</label>
              </div>
              <div className="input-group-text  rounded-end-2">
                <input
                  className="form-check-input"
                  type="checkbox"
                  checked={showPassword}
                  onChange={() => setShowPassword(!showPassword)}
                  aria-label="Checkbox for following input"
                />
              </div>
            </div>
            <div className="container-fluid my-1">
              <Slider {...settings}>
                {photoUrls.map((url) => (
                  <div
                    className="w-100 d-flex  justify-content-center"
                    key={url}
                  >
                    <UserIcon url={url} />
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </form>
      }
      footer={
        <div className="container-fluid fw-bolder fs-6  ">
          <button
            className="btn btn-outline-dark p-2 w-100 h-100"
            type="button"
            aria-label="Login"
            onClick={auth}
          >
            Login
          </button>
        </div>
      }
    />
  );

  function getDataFromForm(data: FormData): Partial<User> {
    return {
      firstName: data.get("name")?.toString().split(" ")[0] as string,
      lastName: data.get("name")?.toString().split(" ")[1] as string,
      birthday: data
        .get("password")
        ?.toString()
        .split(".")
        .reverse()
        .join("-") as string,
      avatar: photoUrls[photoIndex],
    };
  }

  async function sendRequest(promise: Promise<Response>) {
    await promise
      .then(async (res) => {
        if (res.status != 200) throw res;
        return res.json();
      })
      .then((data) => {
        toast.success("Success login", {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });

        setUser(data.user);
        setRole(data.role);

        setTimeout(() => {
          router.replace("/");
        }, 2000);
      })
      .catch(async (res) => {
        const err = (await res?.json()) || " ";
        toast.error(err.message, {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
      });
  }
};

export default Page;
