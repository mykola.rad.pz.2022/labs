import { ChatService } from "../../services/chatService.js";
import { tryCathc } from "../../utils/index.js";
import { Router } from "express";
const router = Router();



router.post("/", async (req, res) => {
  //console.log("req.body", req.body);
  await tryCathc(
    res,
    async () => await ChatService.createChat(req.body)
  );
});

router.put("/", async (req, res) => {
  console.log("req.body update chats", req.body);
  await tryCathc(
    res,
    async () => await ChatService.updateChat(req.body)
  );
});


// body: {userId: string}
router.post("/users/:chatId", async (req, res) => {
  await tryCathc(
    res,
    async () =>
      await ChatService.addUserToChat(req.params.chatId, req.body.userId)
  );
  // TODO: add user for all users online
});


// body: {msgId: string}
router.post("/message/:chatId", async (req, res) => {
  await tryCathc(
    res,
    async () =>
      await ChatService.addMsgToChat(req.params.chatId, req.body.msgId)
  );
  // TODO: add message for all users online
});

// body: {userId: string}
router.delete("/users/:chatId", async (req, res) => {
  await tryCathc(
    res,
    async () =>
      await ChatService.removeUserFromChat(req.params.chatId, req.body.userId)
  );
  // TODO: delete user for all users online
});

// body: {msgId: string}
router.delete("/message/:chatId", async (req, res) => {
  await tryCathc(
    res,
    async () => await ChatService.removeMsgFromChat(req.params.chatId, req.body.msgId)
  );
  // TODO: delete message for all users online
});

router.get("/:userId", async (req, res) => {
  //console.log("req.params.userId", req.params.userId);
  await tryCathc(
    res,
    async () => await ChatService.getAllUserCahts(req.params.userId)
  );
});

export default router;
