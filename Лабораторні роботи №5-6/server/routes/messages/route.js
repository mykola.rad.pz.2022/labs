import { MessageService } from "../../services/messageService.js";
import { MyError, tryCathc } from "../../utils/index.js";
import { Router } from "express";
import { SocketService } from "../../services/socketService.js";
import { ChatService } from "../../services/chatService.js";
import { UserService } from "../../services/userService.js";
const router = Router();

router.get("/:chatId", async (req, res) => {
  await tryCathc(
    res,
    async () =>
    {
      await ChatService.getChat(req.params.chatId).then((chat) => {
        if (!chat.users.find((user) => user._id.toString() == req.session.userId)) {
          throw new MyError("You are not a member of this chat", 403);
        }
      });
     return await MessageService.getAllMessages(
        req.params.chatId,
        req.query.page,
        req.query.limit
      )
    }
  );
});

router.post("/", async (req, res) => {
  let msg = null;
  await tryCathc(
    res,
    async () => (msg = await MessageService.createMessage(req.body))
  );
  if (msg) {
    const chat = await ChatService.getChat(msg.chatId);
    msg = { ...msg._doc, chatName: chat.name, users: chat.users };


    console.log("mssg after addd users\n\n\n\n", msg, msg.users, msg.chatName);

    const offline = SocketService.sendMsgToUsers(
      "new-msg",
      msg,
      chat?.users
        .filter((user) => user._id.toString() != msg.author._id.toString())
        .map((user) => user._id.toString())
    );
    offline.forEach((userId) => {
      UserService.addUnreadMsg(userId, chat._id);
    });
  }
});

router.put("/:id", async (req, res) => {
  await tryCathc(
    res,
    async () => await MessageService.updateMessage(id, req.body)
  );
});

export default router;
