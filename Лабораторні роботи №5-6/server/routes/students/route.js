import { UserService } from "../../services/userService.js";
import { MyError, tryCathc } from "../../utils/index.js";
import { Router } from "express";
const router = Router();

router.get("/search", async (req, res) => {
  //console.log("query", req.query.name, req.query);
  await tryCathc(
    res,
    async () =>
      await UserService.getUsersWithName(
        req.query.name,
        Number.parseInt(req.query.page) || 1
      )
  );
});

router.get("/:id", async (req, res) => {
  await tryCathc(res, async () => await UserService.getUser(req.params.id));
});

router.get("/", async (req, res) => {
  await tryCathc(res, async () => {
    //console.log("req.session.role", req.session.role);
    if (req.session.role == "ADMIN") {
      return await UserService.getAllUsers(Number.parseInt(req.query.page));
    } else {
     // console.log("req.session.userId", req.session.userId);
      return await UserService.getUser(req.session.userId);
    }
  });
});

router.delete("/:id", async (req, res) => {
  await tryCathc(res, async () => {
    if (req.session.role != "ADMIN") {
      throw new MyError("You can't delete user", 403);
    }
    return await UserService.deleteUser(req.params.id);
  });
});

router.put("/:id", async (req, res) => {
  const user = req.body;
 // console.log("user", user);
  // console.log("req.body", req.body);

  await tryCathc(res, async () => {
    if (req.session.role != "ADMIN" && req.session.userId != req.params.id) {
      throw new MyError("You can't change another user", 403);
    }
    return await UserService.updateUser(req.params.id, req.body);
  });
});

router.post("/", async (req, res) => {
  // console.log("req.body", req.body);
  await tryCathc(res, async () => {
    if (req.session.role != "ADMIN") {
      throw new MyError("You can't create user", 403);
    }
    return await UserService.createUser(req.body);
  });
});

export default router;
