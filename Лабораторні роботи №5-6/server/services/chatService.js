import { Chat } from "../mongodb/index.js";
import { ValidationUtil } from "../utils/ValidationUtil.js";

class _ChatService {
  async getAllUserCahts(userId) {
    const chats = await Chat.find({ users: userId })
      .sort({ updatedAt: -1 })
      .select("-messages")
      .populate("users")
      .exec();

    console.log("chats", chats, userId);
    return chats || [];
  }

  async createChat(chat) {
    return await _ChatService.getChatModelFromRequest(chat).save();
  }

  async addUserToChat(chatId, userId) {
    return await Chat.findByIdAndUpdate(chatId, { $push: { users: userId } });
  }

  async removeUserFromChat(chatId, userId) {
    return await Chat.findByIdAndUpdate(chatId, { $pull: { users: userId } });
  }

  async addMsgToChat(chatId, msgId) {
    return await Chat.findByIdAndUpdate(chatId, { $push: { messages: msgId } });
  }

  async removeMsgFromChat(chatId, msgId) {
    return await Chat.findByIdAndUpdate(chatId, { $pull: { messages: msgId } });
  }

  async getChat(chatId) {
    return await Chat.findById(chatId).populate("users").exec();
  }

  async updateChat({_id, users}) {
    return await Chat.findByIdAndUpdate(_id, {users}, {new: true});
  }

  static getChatModelFromRequest(chat) {
    ValidationUtil.validateChat(chat);
    return new Chat({
      name: chat.name,
      users: chat.users,
      messages: chat.messages,
    });
  }
}
export const ChatService = new _ChatService();
