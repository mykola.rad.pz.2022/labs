import { Task } from "../mongodb/index.js";
import { ValidationUtil } from "../utils/ValidationUtil.js";

class _TaskService {
  async getAllTask() {
    return await Task.find();
  }

  async createTask(task) {
    return await _TaskService.getTaskModelFromRequest(task).save();
  }

  async updateTask(id, { name, stage, deadline, description }) {
    _TaskService.getTaskModelFromRequest({
      name,
      stage,
      deadline,
      description,
    });
    return await Task.findByIdAndUpdate(
      id,
      { name, stage, deadline, description },
      { new: true }
    );
  }

  static getTaskModelFromRequest(task) {
    ValidationUtil.validateTask(task);
    return new Task({
      name: task.name,
      stage: task.stage,
      deadline: task.deadline,
      description: task.description,
    });
  }
}

export const TaskService = new _TaskService();
