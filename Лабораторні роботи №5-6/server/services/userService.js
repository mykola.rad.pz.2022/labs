import { User } from "../mongodb/index.js";
import { MyError } from "../utils/index.js";
import { ValidationUtil } from "../utils/ValidationUtil.js";

class _UserService {
  async getAllUsers(page = 1) {
    const limit = 50;
    const skip = (page - 1) * limit;
    return await User.find({}, null, { limit, skip });
  }

  async getUser(id) {
    return await User.findById(id);
  }

  async createUser(user) {
    return await _UserService.getUserModelFromRequestUser(user).save();
  }

  async updateUser(id, user) {
    const u = _UserService.getUserModelFromRequestUser(user);
    u._id = id;
    await User.findByIdAndUpdate(id, u);
    return u;
  }

  async updateUserUnreadMsg(userId, unreadMsg) {
    return await User.findByIdAndUpdate(userId, { unreadMessages: unreadMsg });
  }

  async deleteUser(id) {
    return await User.findByIdAndDelete(id);
  }

  async getUsersWithName(name, page = 1) {
    if (!name) throw new MyError("Не вказано ім'я користувача", 400);
    const limit = 50;
    const skip = (page - 1) * limit;
    const users = await User.find(
      {
        $or: [
          { firstName: { $regex: `.*${name}.*`, $options: "i" } },
          { lastName: { $regex: `.*${name}.*`, $options: "i" } },
        ],
      },
      null,
      { limit, skip }
    );
    return users;
  }

  async addUnreadMsg(userId, chatId) {
    const user = await User.findById(userId);

    const existingUnreadMsgIndex = user.unreadMessages.findIndex(
      (msgId) => msgId.chatId == chatId
    );

    if (existingUnreadMsgIndex === -1) {
      user.unreadMessages.push({ chatId, unread: 1 });
    } else {
      user.unreadMessages[existingUnreadMsgIndex].unread++;
    }

    await user.save();
  }

  async authUser(userPartial) {
    return await User.findOne({
      firstName: userPartial.firstName,
      lastName: userPartial.lastName,
      birthday: userPartial.birthday,
    });
  }

  async updateStatusAndAvatar(userId, status, avatar) {
    return await User.findByIdAndUpdate(userId, { status, avatar });
  }

  static validateRequestUser(user) {
    if (!user) throw new MyError("Некоректні дані користувача", 400);
    ValidationUtil.validateUserName(user.firstName);
    ValidationUtil.validateUserName(user.lastName);
    ValidationUtil.validateUserBirthday(user.birthday);
    ValidationUtil.validateUserGroup(user.group);
    ValidationUtil.validateGender(user.gender);
  }

  static getUserModelFromRequestUser(user) {
    this.validateRequestUser(user);
    return new User({
      firstName: user.firstName,
      lastName: user.lastName,
      group: user.group,
      gender: user.gender,
      birthday: user.birthday,
      status: user.status,
      unreadMessages: user.unreadMessages,
    });
  }
}

export const UserService = new _UserService();
