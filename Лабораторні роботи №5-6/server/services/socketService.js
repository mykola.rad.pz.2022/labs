import { UserService } from "./userService.js";

class _SocketService {
  wss = null;

  init(wss) {
    this.wss = wss;
    this.wss.on("connection", (ws) => {
      ws.on("login", async (message) => {
        const user = await UserService.authUser(await JSON.parse(message));
        if (!user) {
          ws.disconnect();
        } else {
          ws.user = user;
        }
      });
    });
  }

  sendMsgToUsers(typeMsg, msg, usersId = []) {
    let arr = Array.from(usersId);
    this.wss.sockets.sockets.forEach((client) => {
      if (usersId.find((id) => client.user._id.toString() == id.toString())) {
        client.emit(typeMsg, msg);
        arr = arr.filter((id) => id.toString() != client.user._id.toString());
      }
    });
    return arr;
  }

  sendMsgAllUsers(typeMsg, msg) {
    this.wss.sockets.sockets.forEach((client) => {
      client.emit(typeMsg, msg);
    });
  }
}

export const SocketService = new _SocketService();
