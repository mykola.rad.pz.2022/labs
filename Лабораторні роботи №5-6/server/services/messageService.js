import { Message } from "../mongodb/index.js";
import { ValidationUtil } from "../utils/ValidationUtil.js";

class _MessageService {
  async getAllMessages(chatId, page = 1, limit = 10) {
    const skip = (page - 1) * limit;

    if (limit == 1)
      return await Message.find({ chatId }, null, { limit, skip })
        .sort({
          createdAt: -1,
        })
        .populate("author")
        .exec();
    return await Message.find({ chatId }, null, { limit, skip })
      .sort({
        createdAt: -1,
      })
      .exec();
  }

  async getMessage(id) {
    return await Message.findById(id);
  }

  async createMessage(msg) {
    return await _MessageService.getMsgModelFromRequest(msg).save();
  }

  async updateMessage(id, msg) {
    return await Message.findByIdAndUpdate(
      id,
      _MessageService.getMsgModelFromRequest(msg)
    );
  }

  static getMsgModelFromRequest(msg) {
    ValidationUtil.validateMsg(msg);
    return new Message({
      chatId: msg.chatId,
      text: msg.text,
      author: msg.author,
      createdAt: msg.createdAt,
    });
  }
}

export const MessageService = new _MessageService();
