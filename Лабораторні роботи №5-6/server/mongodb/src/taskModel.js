import { Schema, model } from "mongoose";

const taskSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  deadline: {
    type: String,
    required: true,
  },
  stage: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: false,
  },

});

const Task = model("tasks", taskSchema);

export default Task;
