import { Schema, model } from "mongoose";

const unreadMessageSchema = new Schema({
  chatId: { type: String },
  unread: { type: Number },
});

const userSchema = new Schema({
  firstName: {
    type: String,
    required: true,
    minLength: 2,
  },
  lastName: {
    type: String,
    required: true,
    minLength: 2,
  },
  group: {
    type: String,
    required: true,
  },
  gender: {
    type: String,
    enum: ["M", "F"],
    required: true,
  },
  birthday: {
    type: String,
    required: true,
  },
  status: {
    type: Boolean,
    required: true,
    default: false,
  },
  unreadMessages: [unreadMessageSchema],
  avatar: {
    type: String,
  },
});

const User = model("users", userSchema);

export default User;
