import { uri } from "../secret.js";
import mongoose from "mongoose";

mongoose.connect(uri, { useNewUrlParser: true });

const db = mongoose.connection;
console.log("Connecting to database...");

db.on("error", function (err) {
  console.log("Database connection error:", err);
});
db.once("open", function () {
  console.log("Database connected successfully");
});

export default db;
