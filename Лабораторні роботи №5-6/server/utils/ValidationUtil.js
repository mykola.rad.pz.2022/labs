import { MyError } from "./index.js";

export class ValidationUtil {
  static validateUserName(name) {
    if (!name) throw new MyError("The name cannot be empty", 400);

    if (name.length < 2) {
      throw new MyError("The name is too short", 400);
    }

    if (name.length > 50) {
      throw new MyError("The name is too long", 400);
    }

    const pattern = /[A-ZА-ЯІЄЇ][a-z'а-яїіє]{1,30}/u;
    if (!pattern.test(name)) {
      throw new MyError("The name contains invalid characters", 400);
    }

    const chr = name.charAt(0);
    if (chr.toUpperCase() != chr) {
      throw new MyError("The name must start with a capital letter", 400);
    }
  }

  static validateUserBirthday(birthday) {
    try {
      console.log("birthday", birthday);
      const age = new Date().getFullYear() - new Date(birthday).getFullYear();
      if (age <= 16 || age >= 70)
        throw new MyError("You do not meet the age restrictions", 400);
    } catch (error) {
      console.log("error", error);
      throw new MyError("Incorrect date format", 400);
    }
  }

  static validateUserGroup(group) {
    if (!group) throw new MyError("The group cannot be empty", 400);

    const pattern = /PZ-2[1-7]/;
    if (!pattern.test(group)) {
      throw new MyError("Invalid group format", 400);
    }
  }

  static validateGender(gender) {
    if (!["m", "f"].includes(gender.toLowerCase()))
      throw new MyError("The gender is incorrect", 400);
  }

  static validateMsg(msg) {
    if (!msg) throw new MyError("Incorrect data", 400);

    if (!msg.chatId) throw new MyError("Invalid chat ID", 400);

    if (!msg.text)
      throw new MyError("The message text cannot be empty", 400);

    if (!msg.author) throw new MyError("The author of the message is not specified", 400);

    if (!msg.createdAt)
      throw new MyError(
        "The date of creation of the message is not specified",
        400
      );
  }

  static validateChat(chat) {
    if (!chat) throw new MyError("Incorrect data", 400);

    if (!chat.name) throw new MyError("Chat name cannot be empty", 400);

    if (!chat.users)
      throw new MyError("Chat must contain users", 400);
  };

  static validateTask(task) {
    if (!task) throw new MyError("Incorrect data", 400);

    if (!task.name) throw new MyError("The task name cannot be empty", 400);

    if (!task.stage) throw new MyError("The task status cannot be empty", 400);

    if (!task.deadline)
      throw new MyError("The task date cannot be empty", 400);
  }
}
