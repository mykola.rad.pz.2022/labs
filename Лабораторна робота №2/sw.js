// Install service worker
self.addEventListener("install", (event) => {
  console.log("2");
  event.waitUntil(
    caches.open("my-pwa-cache").then((cache) => {
      cache.addAll([
        "index.html",
        "style.css",
        "index_back.js",
        "icons/",
        "screenshots/",
      ]);
    })
  );
});

// Fetch resources with cache fallback
self.addEventListener("fetch", (event) => {
  console.log("3");
  event.respondWith(
    caches.match(event.request).then((response) => {
      if (response) {
        return response;
      }
      return fetch(event.request).then((response) => {
        return caches.open("my-pwa-cache").then((cache) => {
          cache.put(event.request, response.clone());
          return response;
        });
      });
    })
  );
});

// Update cache when new service worker is activated
self.addEventListener("activate", (event) => {
  console.log("4");
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames
          .filter((cacheName) => {
            return (
              cacheName.startsWith("my-pwa-cache-") &&
              cacheName !== "my-pwa-cache-v1"
            );
          })
          .map((cacheName) => {
            return caches.delete(cacheName);
          })
      );
    })
  );
});
