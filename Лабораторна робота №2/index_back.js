var deleteButton;

function validateForm() {
  console.log("5");
  var inputs = [
    document.getElementById("firstName"),
    document.getElementById("lastName"),
    document.getElementById("birthday"),
  ];

  if (
    !validateName(inputs[0]) ||
    !validateName(inputs[1]) ||
    !validateDate(inputs[2])
  ) {
    return false;
  }

  return true;
}

function validateName(input) {
  console.log("7");
  var inputValue = input.value.trim();
  var namePattern = /^[A-Z][a-z'-]*$/;

  if (!namePattern.test(inputValue)) {
    input.classList.add("invalid");
    alert(
      "First character should be capital, no spaces, numbers, and only apostrophe and hyphen allowed!"
    );
    return false;
  } else {
    input.classList.remove("invalid");
  }
  return true;
}

function validateDate(input) {
  var birthdayInput = input.value;
  var birthdayDate = new Date(birthdayInput);
  var currentDate = new Date();
  var age = currentDate.getFullYear() - birthdayDate.getFullYear();

  if (age < 16 || age > 60) {
    input.classList.add("invalid");
    alert("Age must be between 16 and 60 years old!");
    return false;
  } else {
    input.classList.remove("invalid");
  }

  return true;
}

function openForm() {
  console.log("8");
  document.querySelector(".form-header h2").textContent = "Add Student";
  document.querySelector('.button-container input[type="submit"]').value =
    "Create";
  document.getElementById("form-window").style.display = "block";
}

function throwWarning(button) {
  console.log("9");
  var row = button.parentNode.parentNode;
  var cells = row.querySelectorAll("td");
  deleteButton = button;

  var studentName = cells[2].textContent;

  document.getElementById("warning-text").textContent =
    "Are you sure you want to delete user " + studentName + "?";

  document.getElementById("warning").style.display = "block";
}

function closeWarning() {
  console.log("10");
  document.getElementById("warning").style.display = "none";
}

function closeForm() {
  var form = document.querySelector(".add-form");
  console.log("11");
  document.getElementById("form-window").style.display = "none";
  form.reset();
}

document.addEventListener("DOMContentLoaded", function () {
  console.log("12");
  var form = document.querySelector(".add-form");
  var table = document.querySelector(".students-data");

  form.addEventListener("submit", function (event) {
    event.preventDefault();

    if (validateForm()) {
      var isEditing =
        document.getElementById("form-name").textContent == "Edit Student";

      if (isEditing) {
        editStudentData();
      } else {
        addStudentData();
      }
    }
  });

  function addStudentData() {
    console.log("13");
    var id = document.getElementById("custId").value;
    var groupName = document.getElementById("groupInput").value;
    var studentName =
      document.getElementById("firstName").value +
      " " +
      document.getElementById("lastName").value;
    var gender = document.getElementById("genderInput").value;
    var birthday = document.getElementById("birthday").value;

    var newRow = document.createElement("tr");
    newRow.innerHTML = `
      <td><input type="checkbox" /></td>
      <td>${groupName}</td>
      <td>${studentName}</td>
      <td>${gender}</td>
      <td>${birthday}</td>
      <td><i class="fa-solid fa-circle"></i></i></td>
      <td>
        <button onclick="editStudent(this)"><i class="fa-solid fa-pencil"></i></button>
        <button onclick="throwWarning(this)">&#215</button>
      </td>
    `;

    const data = {
      id: id,
      group: groupName,
      name: studentName,
      gender: gender,
      birthday: birthday,
    };

    console.log(JSON.stringify(data));
    const jsonData = JSON.stringify(data);
    // AJAX-запит на сервер
    $.ajax({
      url: "/script.js", // URL для відправки запиту на сервер
      type: "POST",
      contentType: "application/json",
      data: jsonData, // дані для відправки у форматі JSON
      success: function (response) {
        console.log("Запит успішно відправлено");
        // Додаткові дії в разі успішної відправки запиту
      },
      error: function (xhr, status, error) {
        console.error("Виникла помилка при відправці запиту:", error);
        // Додаткові дії в разі помилки
      },
    });

    // Додавання нового рядка до таблиці
    table.appendChild(newRow);

    form.reset();
    // Закриття модального вікна
    closeForm();
  }

  function editStudentData() {
    console.log("14");
    var rowIndex = parseInt(
      document.getElementById("form-window").getAttribute("data-row-index")
    );
    var row = document.querySelector(".students-data").rows[rowIndex];

    // Отримання значень з полів форми
    var groupName = document.getElementById("groupInput").value;
    var studentName =
      document.getElementById("firstName").value +
      " " +
      document.getElementById("lastName").value;
    var gender = document.getElementById("genderInput").value;
    var birthday = document.getElementById("birthday").value;

    // Оновлення даних студента в таблиці
    row.cells[1].textContent = groupName;
    row.cells[2].textContent = studentName;
    row.cells[3].textContent = gender;
    row.cells[4].textContent = birthday;

    form.reset();
    // Скидання форми та закриття модального вікна
    closeForm();
  }
});

function deleteStudent() {
  console.log("15");
  // Отримання батьківського рядка кнопки
  var row = deleteButton.parentNode.parentNode;

  // Видалення студента з таблиці
  row.parentNode.removeChild(row);

  closeWarning();
}

function editStudent(button) {
  console.log("16");
  var row = button.parentNode.parentNode;
  var cells = row.querySelectorAll("td");

  // Отримання даних студента з таблиці
  var groupName = cells[1].textContent;
  var studentName = cells[2].textContent;
  var gender = cells[3].textContent;
  var birthday = cells[4].textContent;

  // Відображення форми для редагування студента
  openForm();

  document
    .getElementById("form-window")
    .setAttribute("data-row-index", row.rowIndex);

  // Заповнення полів форми даними студента
  document.getElementById("groupInput").value = groupName;
  document.getElementById("firstName").value = studentName.split(" ")[0];
  document.getElementById("lastName").value = studentName.split(" ")[1];
  document.getElementById("genderInput").value = gender;
  document.getElementById("birthday").value = birthday;

  // Зміна назви форми та кнопки
  document.querySelector(".form-header h2").textContent = "Edit Student";
  document.querySelector('.button-container input[type="submit"]').value =
    "Save";
}
