var deleteButton;

function openForm() {
  document.querySelector(".form-header h2").textContent = "Add Student";
  document.querySelector('.button-container input[type="submit"]').value =
    "Create";
  document.getElementById("form-window").style.display = "block";
}

function throwWarning(button) {
  var row = button.parentNode.parentNode;
  var cells = row.querySelectorAll("td");
  deleteButton = button;

  var studentName = cells[2].textContent;

  document.getElementById("warning-text").textContent =
    "Are you sure you want to delete user " + studentName + "?";

  document.getElementById("warning").style.display = "block";
}

function closeWarning() {
  document.getElementById("warning").style.display = "none";
}

function closeForm() {
  document.getElementById("form-window").style.display = "none";
}

document.addEventListener("DOMContentLoaded", function () {
  var form = document.querySelector(".add-form");
  var table = document.querySelector(".students-data");

  form.addEventListener("submit", function (event) {
    event.preventDefault(); // Перешкоджає стандартному поведінці форми

    // Перевірка, чи редагуємо студента
    var isEditing =
      document.getElementById("form-name").textContent == "Edit Student";

    if (isEditing) {
      // Виклик другої функції для редагування
      editStudentData();
    } else {
      // Виклик першої функції для додавання
      addStudentData();
    }
  });

  function addStudentData() {
    // Отримання значень з полів форми
    var groupName = document.getElementById("groupName").value;
    var studentName =
      document.getElementById("firstName").value +
      " " +
      document.getElementById("lastName").value;
    var gender = document.getElementById("gender").value;
    var birthday = document.getElementById("birthday").value;

    // Створення нового рядка для таблиці
    var newRow = document.createElement("tr");
    newRow.innerHTML = `
      <td><input type="checkbox" /></td>
      <td>${groupName}</td>
      <td>${studentName}</td>
      <td>${gender}</td>
      <td>${birthday}</td>
      <td><i class="fa-solid fa-circle"></i></i></td>
      <td>
        <button onclick="editStudent(this)"><i class="fa-solid fa-pencil"></i></button>
        <button onclick="throwWarning(this)">&#215</button>
      </td>
    `;

    // Додавання нового рядка до таблиці
    table.appendChild(newRow);

    form.reset();
    // Закриття модального вікна
    closeForm();
  }

  function editStudentData() {
    var rowIndex = parseInt(
      document.getElementById("form-window").getAttribute("data-row-index")
    );
    var row = document.querySelector(".students-data").rows[rowIndex];

    // Отримання значень з полів форми
    var groupName = document.getElementById("groupName").value;
    var studentName =
      document.getElementById("firstName").value +
      " " +
      document.getElementById("lastName").value;
    var gender = document.getElementById("gender").value;
    var birthday = document.getElementById("birthday").value;

    // Оновлення даних студента в таблиці
    row.cells[1].textContent = groupName;
    row.cells[2].textContent = studentName;
    row.cells[3].textContent = gender;
    row.cells[4].textContent = birthday;

    form.reset();
    // Скидання форми та закриття модального вікна
    closeForm();
  }
});

function deleteStudent() {
  // Отримання батьківського рядка кнопки
  var row = deleteButton.parentNode.parentNode;

  // Видалення студента з таблиці
  row.parentNode.removeChild(row);

  closeWarning();
}

function editStudent(button) {
  var row = button.parentNode.parentNode;
  var cells = row.querySelectorAll("td");

  // Отримання даних студента з таблиці
  var groupName = cells[1].textContent;
  var studentName = cells[2].textContent;
  var gender = cells[3].textContent;
  var birthday = cells[4].textContent;

  // Відображення форми для редагування студента
  openForm();
  // document.getElementById("form-window").classList.add("edit-mode");
  document
    .getElementById("form-window")
    .setAttribute("data-row-index", row.rowIndex);

  // Заповнення полів форми даними студента
  document.getElementById("groupName").value = groupName;
  document.getElementById("firstName").value = studentName.split(" ")[0];
  document.getElementById("lastName").value = studentName.split(" ")[1];
  document.getElementById("gender").value = gender;
  document.getElementById("birthday").value = birthday;

  // Зміна назви форми та кнопки
  document.querySelector(".form-header h2").textContent = "Edit Student";
  document.querySelector('.button-container input[type="submit"]').value =
    "Save";
}
